const express = require('express')
var bodyParse = require('body-parser')
const { Router } = require('express')
const app = express()
const PORT = 3000

//iniciar midlleware bodyparser
app.use(express.json())
app.use(bodyParse.urlencoded({extended:true}))
app.use(bodyParse.json())

//criar as rotas
app.use('/user', require('./routers/api/user'))


app.listen(PORT, () => {console.log(`App rodando na porta ${PORT}`)})

