class usuario {
    constructor(nome, email, senha){
        this.id = this.gerarId()
        this.nome = nome
        this.email = email
        this.senha = senha
    }

    gerarId(){
        if(lista_usuarios.length === 0){
            return 1
        }
        return lista_usuarios[lista_usuarios.length - 1].id +1
    }
}

let lista_usuarios = []

lista_usuarios.push(new usuario(1, 'joao', 'joao@infnet.edu', '1234'))
lista_usuarios.push(new usuario(2, 'maria', 'maria@infnet.edu', '4321'))
lista_usuarios.push(new usuario(3, 'felipe', 'felipe@infnet.edu', '7821'))

module.exports = {usuario, lista_usuarios}
