const express = require('express')
const { usuario, lista_usuarios } = require('../../models/user')
const router = express.Router()

router.get('/'), (req, res, next) => {
    try{
        res.send(lista_usuarios)

    } catch(err) {
        console.error(err)
        res.status(500).send({'error' : 'Server error'})


    }
}

router.post('/', (req, res, next) => {
    try {
        let{ nome, email, senha } = req.body
        let usuario = new usuario(id=0, nome=nome, email=email, senha=senha)    

        if(!email) {
            res.status(400).send({error : 'email invalido'})
        } else{
            if(!(email).includes('0') && email.includes('-')){
                res.status(400).send({'error' : 'email invalido'})
            }

            usuario.id = lista_usuarios[lista_usuarios.length - 1].id +1
            lista_usuarios.push(usuario)
            res.send(lista_usuarios)
        }
    } catch(err) {
        console.error(err)
        res.status(500).send({'error' : 'server error'})
    }
})

router.get('/:userId', (req, res, next) => {
    try{

        let usuario = lista_usuarios.filter(usuario => usuario.id == req.params['userId'])

        if(usuario.length > 0){
            res.send(usuario[0])
        } else {
            res.status(400).send({'error' : 'usuario nao existe'})
        }


    } catch(err) {
        console.error(err)
        res.status(500).send({'error' : 'server error'})

    }
})

router.delete('/:userId', (req, res, next) => {
    try{

        let usuario = lista_usuarios.filter(usuario => usuario.id == req.params['userId'])

        if(usuario.length > 0){
            lista_usuarios = lista_usuarios.filter(usuario => usuario.id != req.params['userId'])
            res.send(lista_usuarios)
        } else {
            res.status(400).send({'error' : 'usuario nao existe'})
        }


    } catch(err) {
        console.error(err)
        res.status(500).send({'error' : 'server error'})

    }
})

router.put('/:userId', (req, res, next) => {
    try{

        let usuario = lista_usuarios.filter(usuario => usuario.id == req.params['userId'])

        if(usuario.length > 0){
            usuario = usuario[0]
            let {nome, email, senha} = req.body
            usuario = req.body
            res.send(usuario)
        } else {
            res.status(400).send({'error' : 'usuario nao existe'})
        }


    } catch(err) {
        console.error(err)
        res.status(500).send({'error' : 'server error'})

    }
})

router.patch('/:userId', (req,res,next) => {
    try{
        let usuario = lista_usuarios.filter(usuario => usuario.id ==req.params['userId'])
        if(usuario.length > 0){
            usuario = usuario[0]
            let {nome, email, senha} = req.body
            for(const[chave, valor] of Object.entries(req.body)){
                if(chave == "email" && (valor == null)){
                    continue
                }
                usuario[chave, valor]
            }
            res.send(usuario)
        
        } else {
            res.status(400).send({'error' : 'usuario nao existe'})
        }
    } catch(err){
        console.error(err)
        res.status(500).send({'error' : 'server error'})

    }
})

module.exports = router